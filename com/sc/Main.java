package com.sc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage){
		try{
			Font.loadFont(getClass().getResourceAsStream("Roboto-Thin.ttf"), 28).getName();
	        Font.loadFont(getClass().getResourceAsStream("Roboto-Light.ttf"), 28).getName();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View.fxml"));
			AnchorPane rootLayout = (AnchorPane)loader.load();
			
			//Show the scene containing the root layout
			Scene scene = new Scene(rootLayout);
			
			scene.getStylesheets().clear();
            setUserAgentStylesheet(null);
            scene.getStylesheets()
                 .add(getClass()
                 .getResource("flatred.css")
                 .toExternalForm());
            
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			
			// Give the controller access to the main app.
			((Controller)loader.getController()).setStage(primaryStage);
			primaryStage.setTitle("SlackClerk");
			primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("ca_logo.png")));
			primaryStage.show();
			
		}catch(IOException e){
			e.printStackTrace();
		}
		primaryStage.show();
	}
	
	public static void main(String[] rags){
		launch(rags);
	}
}


class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    
    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }
    
    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println(type + ">" + line);    
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
    }
}
