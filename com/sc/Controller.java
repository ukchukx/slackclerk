package com.sc;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Controller {
	@FXML
	private Button btn;
	@FXML
	private Label lbl;

	@FXML
	private RadioButton web;
	@FXML
	private RadioButton pc;

	@FXML
	private ProgressIndicator progress;
	
	@FXML
	private Label status;

	private ToggleGroup group;

	private Stage stage;

	private Task<String> webWorker, pcWorker;
	private String dirPath,
			webText = "Click the button and wait while the program works.",
			pcText = "Click the button and select the folder containing the data.";

	private static String errorMsg; // do something useful with this

	private static BooleanProperty goodConnection = new SimpleBooleanProperty(
			true);

	private AtomicBoolean busy = new AtomicBoolean();

	void setStage(Stage s) {
		stage = s;

		group = new ToggleGroup();
		pc.setToggleGroup(group);
		web.setToggleGroup(group);
		// make the progress indicator invisible at the start
		progress.setVisible(false);
	}

	@FXML
	private void webClicked() {
		lbl.setText(webText);
	}

	@FXML
	private void pcClicked() {
		lbl.setText(pcText);
	}

	@FXML
	private void handleClick() {
		// Do we have a selection?
		if (group.getSelectedToggle() != null) {
			Toggle t = group.getSelectedToggle();
			if (t == pc) {
				if (!busy.getAndSet(true)) {
					DirectoryChooser chooser = new DirectoryChooser();
					chooser.setTitle("Select data folder");

					File selectedFile = chooser.showDialog(stage);
					pcWorker = getPCThread(selectedFile);
					
					pcWorker.messageProperty().addListener((obs,oV,nV) -> status.setText(nV) );
					
					// set up the progress indicator
					progress.visibleProperty().unbind();
					progress.visibleProperty().bind(pcWorker.runningProperty());
					progress.progressProperty().unbind();
					progress.progressProperty().bind(
							pcWorker.progressProperty());
					// get to work
					new Thread(pcWorker).start();
				}
			} else if (t == web) {
				goodConnection.set(ping());
				if (!ping()) {
					errorMsg = "Slack could not be reached."
							+ System.lineSeparator()
							+ "Check internet connectivity.";
					System.out.println(errorMsg);
					return;
				}

				String dir = System.getProperty("user.home")
						+ System.getProperty("file.separator") + "Desktop";// Windows
																			// obviously
				
				System.out.println("Our save directory: "+dir);
				webWorker = getWebThread(new File(dir));
				
				webWorker.messageProperty().addListener((obs,oV,nV) -> status.setText(nV) );
				
				// set up the progress indicator
				progress.visibleProperty().unbind();
				progress.visibleProperty().bind(webWorker.runningProperty());
				progress.progressProperty().unbind();
				progress.progressProperty().bind(webWorker.progressProperty());
				// get to work
				new Thread(webWorker).start();
			}
		}
	}

	private static String docFolder = "";
	private static Date d = new Date();

	//******* Web section **********
	private Task<String> getWebThread(File dir) {
		System.out.println("Entered getWebThread()");
		return new Task<String>() {
			@Override
			protected String call() {
				updateMessage("Setting things up...");
				stage.setOnCloseRequest(e -> e.consume());
				
				d.setMonth(6);//**
				
				String folderName[] = d.toString().split(" ");
				
				updateMessage("Creating the folder to store the reports...");

				File reportFolder = new File(dir.getAbsolutePath()
						+ System.getProperty("file.separator")
						+ "Slack_Reports-" + folderName[1] + "_"
						+ folderName[5]);
				reportFolder.mkdir();

				docFolder = reportFolder.getAbsolutePath();
				
				System.out.println("Reports to be saved in: "+docFolder);
				
				updateMessage("Getting the list of users...");

				buildUserList(null, false);
				
				updateMessage("Getting the list of channels...");
				
				buildChannelList();
				
				d.setDate(31);//**
				
				for (int i = d.getDate(); i >= 1; i--) { //Prepare on the last day, so it can count down without skipping any day
					updateMessage("Preparing report for day "+i+"...");
					getDay(i);
				}
				
				// TODO
				// Add some error handling
				
				updateMessage("Done");
				
				System.out.println("Exited getWebThread()");
				Platform.runLater(() -> {
					busy.set(false);
					status.setText(null);
					stage.setOnCloseRequest(e -> stage.close());
					showFiles(docFolder);
				});
				
				return "Success";
			}

		};
	}

	private void getDay(int day) {
		Date tmp = new Date();tmp.setMonth(6);
		tmp.setDate(day);// Set the day

		String file = (tmp.getYear() + 1900) + "-" + (tmp.getMonth() + 1) + "-"
				+ tmp.getDate();
		System.out.println(file);

		// get the timestamp for 00:00
		tmp.setHours(0);
		tmp.setMinutes(0);
		tmp.setSeconds(0);
		long oldestTS = (tmp.getTime() / 1000);

		// get the timestamp for 23:59
		tmp.setHours(23);
		tmp.setMinutes(59);
		tmp.setSeconds(0);
		long latestTS = (tmp.getTime() / 1000);

		// Create a Map to hold all channel messages
		Map<String, String> msgs = new TreeMap<>();

		for (String[] channel : channels) {
			try {
				String header = System.lineSeparator() + "# "
							+ channel[1] + System.lineSeparator()
							+ System.lineSeparator();
				StringBuilder messages = new StringBuilder();

				boolean waitThere_sMore = false, v = true;
				Map<String,Object> res = new TreeMap<>();
				
				do{
					res = readMessagesFromChannel(channel[0], latestTS, oldestTS);
					waitThere_sMore = (boolean) res.getOrDefault("more", false);
					oldestTS = ((long) res.getOrDefault("lastTS", oldestTS)) + 1;
					String msg = (String) res.getOrDefault("messages", "");
					
					if(v){
						messages.append(header+msg);
						v = false; //No more v
					}else{
						messages.append(msg);
					}
					
					if(waitThere_sMore){
						System.out.println("Na wa o: "+channel[1]);
						System.out.println("New oldest ts: "+ oldestTS);
					}
				}while(waitThere_sMore);
				
				msgs.put(channel[0], messages.toString());
				

			} catch (Exception e) {
				errorMsg = e.getMessage();
				e.printStackTrace();
			}
		}
		
		// Done for the day, create file
		try {
			File reportFile = new File(docFolder
					+ System.getProperty("file.separator") + file + ".txt");

			BufferedWriter bw = new BufferedWriter(new FileWriter(reportFile));
			for (String[] channel : channels) {
				String m = msgs.getOrDefault(channel[0], "");
				if (!m.isEmpty()) {
					System.out.println("Writing channel :" + channel[1]);
					bw.write(m);
					bw.flush();
				}

			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 * @param chCode
	 * @param latestTS
	 * @param oldestTS
	 * @returns An object array
	 * [] - are there more messages
	 * [] - the ts of the last message
	 * [] - the compiled messages
	 */
	private Map<String,Object> readMessagesFromChannel(String ch,long latestTS,long oldestTS){
		Map<String,Object> result = new TreeMap<>();
		
		try{
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(
					"https://slack.com/api/channels.history");
			List<NameValuePair> data = new ArrayList<>();
			data.add(new BasicNameValuePair("token",
					"xoxp-3966212417-4940157210-7204384148-ec6c54"));
			data.add(new BasicNameValuePair("channel", ch));
			data.add(new BasicNameValuePair("latest", latestTS + ""));
			data.add(new BasicNameValuePair("oldest", oldestTS + ""));
			data.add(new BasicNameValuePair("inclusive", "1"));
			data.add(new BasicNameValuePair("count", "1000"));
			
			post.setEntity(new UrlEncodedFormEntity(data));
			HttpResponse response = client.execute(post);
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = "", jsonText = "";
			
			//Get our stuff from the connection
			while ((line = rd.readLine()) != null) {
				jsonText += line;
			}
			
			JSONObject json = (JSONObject) (new JSONParser()
			.parse(jsonText));
			if (json.get("ok").toString().equals("true")) {
				
				result.put("more", (json.get("has_more").toString()
						.equals("true")) ? true : false);
				
				long nextTS = 0;
				String message = "";
		
				JSONArray j = (JSONArray) new JSONParser().parse(json.get("messages").toString());
		
				for (Object o : j) { // for each message
					JSONObject q = (JSONObject) o;
					// get the time
					String time = q.getOrDefault("ts", "@notime@")
							.toString();
					long ts = 0;
					if (!time.equals("@notime@")) {
						ts = Long.parseLong(time.split("\\.")[0]);
					}
		
					
					 if(ts > nextTS){ //Store the bigger timestamp 
						 nextTS	 = ts; 
					 }
					 
		
					// get the other stuff
					String text = q.get("text").toString();
					String user = q.getOrDefault("user", "@nouser@")
							.toString();
					String sub = q.getOrDefault("subtype", "@none@")
							.toString();
					String msgTime = "";
					if (ts > 0) {
						msgTime = new Date(ts * 1000).toString().split(" ")[3];
					}
		
					// Prepare the message entry
					String entry = "";
					if (!user.equals("@nouser@") && sub.equals("@none@")) {
						entry = (msgTime.isEmpty() ? "" : msgTime + " ")
								+ user + " : " + text
								+ System.lineSeparator();
						entry = StringUtils.replaceEach(entry, codes,usernames);
					} else if (sub.equals("file_share")) {
						entry = (msgTime.isEmpty() ? "" : msgTime + " ")
								+ " *** " + text + System.lineSeparator();
						
						for (String username : usernames) {
							entry = StringUtils.replace(entry, "|"
									+ username + ">", ">");
						}
					}
					
					// Append to message string
					message += entry;
				}
				
				result.put("lastTS", nextTS);
				result.put("messages", message);
			}
			
			
			return result;
		}catch(Exception e){
			return null;
		}
		
	}

	@SuppressWarnings("unchecked")
	private void buildChannelList() {
		try {
			if (!goodConnection.get()) {
				errorMsg = "Slack could not be reached."
						+ System.lineSeparator()
						+ "Check internet connectivity.";
				return;
			}
			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost("https://slack.com/api/channels.list");
			List<NameValuePair> data = new ArrayList<>();
			data.add(new BasicNameValuePair("token",
					"xoxp-3966212417-4940157210-7204384148-ec6c54"));
			post.setEntity(new UrlEncodedFormEntity(data));

			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = "", jsonText = "";
			while ((line = rd.readLine()) != null) {
				jsonText += line;
			}
			// Make our json look familiar
			JSONObject json = (JSONObject) (new JSONParser().parse(jsonText));
			if (json.get("ok").toString().equals("true")) {

				JSONArray j = (JSONArray) new JSONParser().parse(json.get(
						"channels").toString());

				j.forEach(o -> {
					JSONObject q = (JSONObject) o;

					String id = q.get("id").toString();
					String name = q.get("name").toString();
					channels.add(new String[] { id, name });

				});
				chCode = new String[channels.size()];
				chName = new String[channels.size()];
				int i = 0;
				for (String[] channel : channels) {
					chCode[i] = channel[0];
					chName[i] = channel[1];
					++i;
				}
			}

		} catch (Exception e) {

		}
	}

	private static boolean ping() {
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(
					"http://slack.com").openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setRequestMethod("HEAD");
			connection.setRequestProperty("Accept-Encoding", "musixmatch");
			int responseCode = connection.getResponseCode();
			return (200 <= responseCode && responseCode <= 399);
		} catch (IOException exception) {
			return false;
		}
	}
	
	//********** PC section ************
	private static void crawl(File f) {
		System.out.println("Entered crawl()");
		if (!f.isDirectory()) {
			return;
		}
		List<File> channels = Arrays.asList(f.listFiles()), dirs = new ArrayList<>();
		List<File>[] dayArray = new LinkedList[32];
		
		//Initialize holders
		for (int l = 0; l < dayArray.length; l++) {
			dayArray[l] = new LinkedList<File>();
		}

		buildUserList(
				new File(f.getAbsoluteFile()
						+ System.getProperty("file.separator") + "users.json"),
				true);
		
		//Get all channels (i.e. directories)
		for (int i = 0; i < channels.size(); i++) {
			if (channels.get(i).isDirectory()) {
				dirs.add(channels.get(i));
			}
		}
		
		// Get all files in the channels
		List<File> days = null;
		for (File channel : dirs) {
			days = Arrays.asList(channel.listFiles());
			days.forEach((File day) -> {
				//Get only files for the current month
				String fName = day.getName().split("\\.")[0];
				int dy = Integer.parseInt(fName.split("-")[2]);
				int mnth = Integer.parseInt(fName.split("-")[1]);
				if (mnth == d.getMonth() + 1) {
					dayArray[dy].add(day);
				}
			});
		}
		
		int i = 0;
		for (List<File> dayRecords : dayArray) {
			if (dayRecords.size() != 0) {
				System.out.println("Day " + i);
				prepReport(dayRecords);
			}
			++i;
		}
		System.out.println("Exited crawl()");
	}

	private static String returnFileContents(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1); // haha, in your face, return!
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private static void prepReport(List<File> dayChat) {
		File reportFile = new File(docFolder
				+ System.getProperty("file.separator")
				+ dayChat.get(0).getName().split("\\.")[0] + ".txt");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(reportFile))) {
			reportFile = new File(docFolder
					+ System.getProperty("file.separator")
					+ dayChat.get(0).getName().split("\\.")[0] + ".txt");

			// BufferedWriter bw = new BufferedWriter(new
			// FileWriter(reportFile));
			for (File file : dayChat) {
				System.out.println(file.getAbsolutePath());
				// write channel
				writer.write(System.lineSeparator() + "      #"
						+ file.getParentFile().getName()
						+ System.lineSeparator() + System.lineSeparator());

				String everything = returnFileContents(file);

				JSONArray j = (JSONArray) new JSONParser().parse(everything);

				j.forEach((o) -> {
					JSONObject q = (JSONObject) o;

					String text = q.get("text").toString();
					String user = q.getOrDefault("user", "@nouser@").toString();
					String sub = q.getOrDefault("subtype", "@none@").toString();
					String time = q.getOrDefault("ts", "@notime@").toString();
					String currentTime = null;
					Date d = null;
					if (!time.equals("@notime@")) {
						long ts = Long.parseLong(time.split("\\.")[0]);
						d = new Date(ts * 1000);
						currentTime = d.toString().split(" ")[3];// get the time
																	// component
					}

					// Prepare the entry
					String entry = "";

					if (!user.equals("@nouser@") && sub.equals("@none@")) {
						entry = (currentTime.isEmpty() ? "" : currentTime + " ")
								+ user + " : " + text + System.lineSeparator();
						entry = StringUtils
								.replaceEach(entry, codes, usernames);

					} else if (sub.equals("file_share")) {
						entry = (currentTime.isEmpty() ? "" : currentTime + " ")
								+ " *** " + text + System.lineSeparator();
						entry = StringUtils
								.replaceEach(entry, codes, usernames);
						for (String username : usernames) {
							entry = StringUtils.replace(entry, "|" + username
									+ ">", ">");
						}

					}
					try {
						writer.write(entry);
						writer.flush();
					} catch (IOException e) {
						e.printStackTrace();
						System.exit(0);
					}

				});
				//writer.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	private Task<String> getPCThread(File dir) {
		System.out.println("Entered getPCThread()");
		// d.setMonth(5);//temporary setting 'cos we need June.
		if (dir == null)
			return null;
		return new Task<String>() {
			@Override
			protected String call() {
				updateMessage("Setting things up...");
				stage.setOnCloseRequest(e -> e.consume());

				dirPath = dir.getParent();
				String folderName[] = d.toString().split(" ");
				docFolder = dirPath + System.getProperty("file.separator")
						+ folderName[5] + "-" + folderName[1];
				new File(docFolder).mkdir();
				updateMessage("Preparing reports from the selected folder...");
				crawl(dir);
				

				updateMessage("Done");
				
				System.out.println("Exited getPCThread()");
				Platform.runLater(() -> {
					busy.set(false);
					status.setText(null);
					stage.setOnCloseRequest(e -> stage.close());
					showFiles(docFolder);
				});
				
				return "Success";
			}
		};
	}
	
	//******* Commons ***********
	private static List<String[]> users = new ArrayList<>(),
			channels = new ArrayList<>();
	private static String[] codes, usernames, chCode, chName;

	@SuppressWarnings("unchecked")
	private static void buildUserList(File f, boolean local) {
		System.out.println("Entered buildUserList(" + (f==null?"null":f.getName()) + "," + local + ")");
		if (local) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				JSONArray j = (JSONArray) new JSONParser().parse(everything);

				j.forEach(o -> {
					JSONObject q = (JSONObject) o;

					String id = q.get("id").toString();
					String name = q.get("name").toString();
					users.add(new String[] { id, name });

				});
				codes = new String[users.size()];
				usernames = new String[users.size()];
				int i = 0;
				for (String[] user : users) {
					// System.out.println(user[0]+" "+user[1]);
					codes[i] = user[0];
					usernames[i] = user[1];
					++i;
				}
				// System.exit(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				if (!ping()) {
					errorMsg = "Slack could not be reached."
							+ System.lineSeparator()
							+ "Check internet connectivity.";
					System.out.println(errorMsg);
					return;
				}
				CloseableHttpClient client = HttpClients.createDefault();
				HttpPost post = new HttpPost("https://slack.com/api/users.list");
				List<NameValuePair> data = new ArrayList<>();
				data.add(new BasicNameValuePair("token",
						"xoxp-3966212417-4940157210-7204384148-ec6c54"));
				post.setEntity(new UrlEncodedFormEntity(data));
				
				System.out.println("Trying to get user list.");

				HttpResponse response = client.execute(post);
				BufferedReader rd = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				String line = "", jsonText = "";
				while ((line = rd.readLine()) != null) {
					jsonText += line;
				}
				// Make our json look familiar
				JSONObject json = (JSONObject) (new JSONParser()
						.parse(jsonText));
				if (json.get("ok").toString().equals("true")) {
					System.out.println("We got good stuff in the users department.");

					JSONArray j = (JSONArray) new JSONParser().parse(json.get(
							"members").toString());

					j.forEach(o -> {
						JSONObject q = (JSONObject) o;

						String id = q.get("id").toString();
						String name = q.get("name").toString();
						users.add(new String[] { id, name });

					});
					codes = new String[users.size()];
					usernames = new String[users.size()];
					int i = 0;
					for (String[] user : users) {
						System.out.println(user[0] + " "+user[1]);
						codes[i] = user[0];
						usernames[i] = user[1];
						++i;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Exited buildUserList(" + (f==null?"null":f.getName()) + "," + local + ")");
		
			
	}

	private void showFiles(String folder) {
		Desktop desktop = null;
		File file = new File(folder);
		if (Desktop.isDesktopSupported()) {
			desktop = Desktop.getDesktop();
		}
		try {
			desktop.open(file);
		} catch (IOException e) {
		}
	}
}
